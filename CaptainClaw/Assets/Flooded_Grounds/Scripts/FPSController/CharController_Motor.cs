﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController_Motor : MonoBehaviour
{

    public float speed = 3.0f;
    public float sensitivity = 15.0f;
    public float WaterHeight = 15.5f;
    public float runSpeed = 5.0f;
    public float walkSpeed = 3.0f;
    CharacterController character;
    public GameObject cam;
    float moveFB, moveLR;
    float rotX, rotY;
    public bool webGLRightClickRotation = true;
    float gravity = -9.8f;
    private Animator animator;
    private float jumpHeight = -370.5f;
    private Vector3 movement;
    void Start()
    {
        //LockCursor ();
        character = GetComponent<CharacterController>();
        if (Application.isEditor)
        {
            webGLRightClickRotation = false;
            sensitivity = sensitivity * 1.5f;
        }
        animator = GetComponentInChildren<Animator>();
    }


    void Update()
    {
        Move();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            StartCoroutine(Attack());
        }
    }


    void CameraRotation(GameObject cam, float rotX, float rotY)
    {
        transform.Rotate(0, rotX * Time.deltaTime, 0);
        cam.transform.Rotate(-rotY * Time.deltaTime, 0, 0);
    }


    private IEnumerator Attack()
    {
        animator.SetLayerWeight(animator.GetLayerIndex("Attack Layer"), 1);
        animator.SetTrigger("Attack1");

        yield return new WaitForSeconds(0.9f);
        animator.SetLayerWeight(animator.GetLayerIndex("Attack Layer"), 0);

    }

    private void Move()
    {
        moveFB = Input.GetAxis("Horizontal") * speed;
        moveLR = Input.GetAxis("Vertical") * speed;

        rotX = Input.GetAxis("Mouse X") * sensitivity;
        rotY = Input.GetAxis("Mouse Y") * sensitivity;

        movement = new Vector3(moveFB, gravity, moveLR);

        if (webGLRightClickRotation)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                CameraRotation(cam, rotX, rotY);
            }
        }
        else if (!webGLRightClickRotation)
        {
            CameraRotation(cam, rotX, rotY);
        }
        movement = transform.rotation * movement;
        Vector3 moveDirection = new Vector3(0, gravity, 0);

        if (movement == moveDirection)
        {
            animator.SetFloat("Speed", 0.0f);

        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetFloat("Speed", 0.5f);
            Run();
        }
        else
        {
            animator.SetFloat("Speed", 0.25f);
            Walk();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
        character.Move(movement * Time.deltaTime);

    }
    private void Run()
    {
        speed = runSpeed;
    }
    private void Walk()
    {
        speed = walkSpeed;
    }
    private void Jump()
    {
        movement.y = Mathf.Sqrt(jumpHeight * gravity);
    }

}

