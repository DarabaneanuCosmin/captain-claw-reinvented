using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerHealth;

public class Dragon : MonoBehaviour
{
 Animator dragonAni;
 public Transform target;
 public float dragonSpeed;
 //public Transform Spawnpoint;
 public Rigidbody Prefab;
 public Player player;

 

 bool enableAct;
 int atkStep;
 int dragonHealth;


 private void Start()
 {
     dragonAni= GetComponent<Animator>();
     enableAct=true;  //actio switch 
     dragonHealth = 5;

 }

 //function that makes the dragon look at the target  
 void RotateDragon()
 {
     Vector3 dir=target.position-transform.position;
     transform.localRotation=
        Quaternion.Slerp(transform.localRotation,
            Quaternion.LookRotation(dir),5*Time.deltaTime);
 }

 void MoveDragon()
 {
     if((target.position-transform.position).magnitude >= 10 && (target.position-transform.position).magnitude <= 30)
     {
         dragonAni.SetBool("Walk",true);
         transform.Translate(Vector3.forward*dragonSpeed
            * Time.deltaTime, Space.Self);
     }

     if((target.position-transform.position).magnitude<10)
     {
         dragonAni.SetBool("Walk",false);
     }
 }

 private void Update()
 {
     if(enableAct)
     {
         RotateDragon();
         MoveDragon();
     }
    // if(dragonHealth <= 0){
    //     // dragonAni.Play("Die");
    //      Destroy(this.gameObject);
    // }
 }

 void DragonAtk()
 {
     if((target.position-transform.position).magnitude<10)
     {
         switch(atkStep)
         {
             case 0:
                atkStep+=1;
                dragonAni.Play("Basic Attack");
                break;
            case 1:
                atkStep+=1;
                dragonAni.Play("Claw Attack");
                break;
            case 2:
                atkStep=0;
                dragonAni.Play("Flame Attack");
                break;
         }
     }
 }

void FreezeDragon()
{
    // enableAct=false;
}

void UnFreezeDragon()
{
    // enableAct=true;

}

IEnumerator OnTriggerEnter(Collider obstacle)

    {
		Debug.Log("Object tag is: " + gameObject.tag);
		Debug.Log("obstacle has the tag: " + obstacle.gameObject.tag);
		if (obstacle.gameObject.tag == "Weapon" && Input.GetMouseButtonDown(0))
		{
			Debug.Log("dragon got hit");
			// currentHealth -= 5;
			dragonHealth -= player.damage;
            dragonAni.Play("Get Hit");
			Debug.Log("health after damage: " + dragonHealth);
			Debug.Log("damage taken");
            if(dragonHealth<=0)
            {
                dragonAni.Play("Die");
                yield return new WaitForSeconds(5);
                enableAct=false;
                
                // GetComponent<Renderer>().enabled = false;
                Destroy(this.gameObject);
                
                Rigidbody RigidPrefab;

                RigidPrefab = Instantiate(Prefab, this.gameObject.transform.position, Quaternion.Euler(-90, 90, 45)) as Rigidbody;
            }
		}

    }

}
