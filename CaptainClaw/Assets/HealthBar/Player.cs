using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PlayerHealth
{
    public class Player : MonoBehaviour
    {

        public int maxHealth = 100;
        public int currentHealth;
        public HealthBar healthBar;
        public int damage =5;
        public GameObject treasureText;
        
        

        // Start is called before the first frame update
        void Start()
        {
            currentHealth = maxHealth;
            healthBar.SetMaxHealth(maxHealth);
            treasureText.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            // if (Input.GetKeyDown(KeyCode.Tab))
            // {
            // 	TakeDamage(20);
            // }
        }

        public void TakeDamage(int damage)
        {
            currentHealth -= damage;

            healthBar.SetHealth(currentHealth);
        }

        public void Heal(int heal)
        {
            currentHealth += heal;

            healthBar.SetHealth(currentHealth);
        }


        IEnumerator OnTriggerEnter(Collider obstacle)

        {

            if (obstacle.gameObject.tag == "Enemy" || obstacle.gameObject.tag == "Snowball")
            {

                TakeDamage(5);
            }

            if (obstacle.gameObject.tag == "Potion")
            {

                Heal(5);
                Destroy(obstacle.gameObject);
            }

            if (obstacle.gameObject.tag == "treasure")
            {

                damage+=5;
                Debug.Log("New damage = "+damage);
                Destroy(obstacle.gameObject);

                

                treasureText.SetActive(true);
                yield return new WaitForSeconds(5);
                Destroy(treasureText);
                
            }

            if (obstacle.gameObject.tag == "Dragon")
            {

                TakeDamage(10);
            }

            if (obstacle.gameObject.tag == "stake")
            {

                TakeDamage(20);
            }
        }

    }

}