using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    
    [SerializeField] Slider volumeSlider;
    public void PlayGame()
    {
            SceneManager.LoadScene("Scene_A");
    }

    public void QuitGame()
    {   
        Debug.Log("Quit");
        Application.Quit();
    }

    public void ChangeVolume()
    {
        AudioListener.volume = volumeSlider.value;
        Save();
    }

    public void Save()
    {
        PlayerPrefs.SetFloat("3D_WindHowl", volumeSlider.value);
    }
}
