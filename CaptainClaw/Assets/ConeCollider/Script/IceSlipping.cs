using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceSlipping : MonoBehaviour
{
    GameObject player;
    Vector3 move_player_coordinates;
    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");
        move_player_coordinates = new Vector3(28.7f, 1.1f, -21.2f);
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            player.transform.position = move_player_coordinates;
        }
    }
}
