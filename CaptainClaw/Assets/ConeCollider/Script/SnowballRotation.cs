using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowballRotation : MonoBehaviour
{
    private Rigidbody body;
    private float speed;


    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        speed = 5.0f;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {

        if (GameObject.FindGameObjectWithTag("Snowball").transform.position.z >= -72.45f)
        {
            body.AddForce(new Vector3(0.0f, 0.0f, 1.0f - transform.position.z) * speed * Time.deltaTime);
        }
        else if (GameObject.FindGameObjectWithTag("Snowball").transform.position.z == 0.0f)
        {
            body.AddForce(new Vector3(0.0f, 0.0f, 1.0f - transform.position.z) * speed * Time.deltaTime);
        }
        else if (GameObject.FindGameObjectWithTag("Snowball").transform.position.z <= 0.1f)
        {
            body.AddForce(new Vector3(0.0f, 0.0f, 1.0f + transform.position.z) * speed * Time.deltaTime);
        }
    }
}
