using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SingleSpike
{
    public class SingleSpike : MonoBehaviour
    {
        public void Start()
        {
        }
        public void Update()
        {
            Shoot();
            Retract();
        }
        public void Shoot()
        {
            if (this.transform.position.y <= 0.1f)
            {
                StartCoroutine(_Shoot());
            }   
        }
        IEnumerator _Shoot()
        {
            Vector3 cameraOffset = new Vector3(0.0f, 0.05f, 0.0f);
            yield return new WaitForSeconds(0.36f);
            this.transform.position += (cameraOffset);
        }
        public void Retract()
        {
            if (this.transform.position.y >= 0.1f)
            {
                StartCoroutine(_Retract());
            }
            
        }
        IEnumerator _Retract()
        {
            Vector3 cameraOffset = new Vector3(0.0f, 0.05f, 0.0f);
            yield return new WaitForSeconds(0.36f);
            this.transform.position -= (cameraOffset);
        }
      
    }
  
}

