using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stakemove : MonoBehaviour
{
    public bool left = true;
    private Vector3 dir = Vector3.left;
    public float speed = 4.0f;
    public float positionRight = -65.0f;
    public float positionLeft = -60.0f;
    void Start()
    {
        if (left)
        {
            dir = Vector3.left;
        }
        else
        {
            dir = Vector3.right;

        }
    }

    //Your Update function
    void Update()
    {
        gameObject.transform.Translate(dir * speed * Time.deltaTime);

        if (gameObject.transform.position.x <= positionRight)
        {
            dir = Vector3.right;
        }
        else if (gameObject.transform.position.x >= positionLeft)
        {
            dir = Vector3.left;
        }
    }
}
